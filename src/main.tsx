import './polyfills';
import './index.css';

import React from 'react';
import { createRoot } from 'react-dom/client';
import { ErrorBoundary } from 'react-error-boundary';

import { HotkeysProvider } from '@blueprintjs/core';
import localforage from 'localforage';

import { App } from './components/app/app';
import { _dummyTableData } from './data/_dummy-data';
import DataStorageModule from './modules/data-storage/data-storage';
import EquationsEngine from './modules/equations-engine/equations-engine';
import TableComputationsService from './modules/table-computations-service/table-computations-service';
import { TableDataProvider } from './providers/table-data-provider/table-data-provider';

const preloaderComponent = document.getElementById('preloader') as HTMLElement;
const preloaderHTML = { __html: preloaderComponent.outerHTML };
const root = createRoot(document.getElementById('root') as HTMLElement);

const dataStorageModule = new DataStorageModule(localforage, _dummyTableData);
// starting fetching the data before rendering
void dataStorageModule.fetchData();

const equationsEngine = new EquationsEngine();
const tableComputationsService = new TableComputationsService(equationsEngine);

// TODO temporarily for debugging
window._dataStorageModule = dataStorageModule;
window._equationsEngine = equationsEngine;
window._tableComputationsService = tableComputationsService;

// TODO move to some sort of config
const tableSyncIntervalInMs = 1000;

// TODO use React.StrictMode after https://github.com/palantir/blueprint/issues/3979 is fixed
root.render(
    <React.Fragment>
        {/*TODO implement better error boundary fallback*/}
        {/*TODO error logging*/}
        <ErrorBoundary fallback={<div>Something went wrong</div>}>
            <React.Suspense fallback={<div dangerouslySetInnerHTML={preloaderHTML} />}>
                <TableDataProvider
                    dataStorageModule={dataStorageModule}
                    tableComputationsService={tableComputationsService}
                    tableSyncIntervalInMs={tableSyncIntervalInMs}
                >
                    <HotkeysProvider>
                        <App />
                    </HotkeysProvider>
                </TableDataProvider>
            </React.Suspense>
        </ErrorBoundary>
    </React.Fragment>,
);
