import * as React from 'react';
import {
    PropsWithChildren,
    useCallback,
    useContext,
    useEffect,
    useMemo,
    useSyncExternalStore,
} from 'react';

import { useDebounce, useEventListener } from 'usehooks-ts';

import DataStorageModule from '../../modules/data-storage/data-storage';
import type { IStorageData } from '../../modules/data-storage/types';
import TableComputationsService from '../../modules/table-computations-service/table-computations-service';
import {
    ColumnAggregation,
    ColumnHeader,
    DisplayDimensions,
    StartEditColumnConfig,
    TableValues,
} from '../../modules/table-computations-service/types';

export type OnCellValueEdit = (
    value: string,
    place: { rowIndex: number; columnIndex: number },
) => void;
export type OnColumnRename = (value: string, columnIndex: number) => void;
export type OnDeleteColumn = (columnIndex: number) => void;
export type OnAddNewRow = () => void;
export type OnDeleteRow = (rowIndex: number) => void;
export type OnResetData = () => void;
export type OnSubmitColumn = (values: StartEditColumnConfig['values'], columnIndex: number) => void;

export interface TableDataContextState {
    isSaving: boolean;

    headline: ColumnHeader[];
    grid: TableValues;
    aggregations: Array<ColumnAggregation[]>;
    valuesDimensions: DisplayDimensions;
    aggregationsDimensions: DisplayDimensions;

    onCellValueEdit: OnCellValueEdit;
    onColumnRename: OnColumnRename;
    onAddNewRow: OnAddNewRow;
    onDeleteRow: OnDeleteRow;
    onDeleteColumn: OnDeleteColumn;
    onResetData: OnResetData;
    onSubmitColumn: OnSubmitColumn;
}

interface InternalTableDataContextState {
    dataStorageModule: DataStorageModule;
    tableComputationsService: TableComputationsService;
}

const TABLE_DATA_CONTEXT_DEFAULT_VALUE: TableDataContextState = {
    isSaving: false,

    headline: [],
    grid: [],
    aggregations: [],
    valuesDimensions: { cols: 0, rows: 0 },
    aggregationsDimensions: { cols: 0, rows: 0 },

    onCellValueEdit: () => {},
    onColumnRename: () => {},
    onAddNewRow: () => {},
    onDeleteRow: () => {},
    onDeleteColumn: () => {},
    onResetData: () => {},
    onSubmitColumn: () => {},
};
const TableDataContext = React.createContext<TableDataContextState>(
    TABLE_DATA_CONTEXT_DEFAULT_VALUE,
);
const InternalTableDataContext = React.createContext<InternalTableDataContextState | null>(null);

interface TableDataProviderProps {
    dataStorageModule: DataStorageModule;
    tableComputationsService: TableComputationsService;
    tableSyncIntervalInMs: number;
}

export const TableDataProvider: React.FC<PropsWithChildren<TableDataProviderProps>> = ({
    dataStorageModule,
    tableComputationsService,
    tableSyncIntervalInMs,
    children,
}) => {
    if (!dataStorageModule.isInitialFetchDone()) {
        // Throwing a React.Suspense compatible promise
        // TODO check it still works when upgrading React version
        throw dataStorageModule.fetchData().then(() => {
            const [storageData] = dataStorageModule.getSnapshot();
            return tableComputationsService.update(storageData);
        });
    }

    // setting up refetching when storage changed in another tab
    useEventListener<'storage'>('storage', () => {
        void dataStorageModule.fetchData();
    });

    const [tableData, isSaving] = useSyncExternalStore(
        dataStorageModule.subscribe,
        dataStorageModule.getSnapshot,
    );
    const computedTable = useSyncExternalStore(
        tableComputationsService.subscribe,
        tableComputationsService.getSnapshot,
    );
    const { headline, grid, aggregations, valuesDimensions, aggregationsDimensions } =
        computedTable;
    const debouncedComputedTable = useDebounce(computedTable, tableSyncIntervalInMs);

    useEffect(() => {
        tableComputationsService.update(tableData);
    }, [tableData]);

    const updateData = useCallback(
        (data: IStorageData) => {
            void dataStorageModule.updateData(data);
        },
        [dataStorageModule],
    );

    // save updated values to the storage after all computations are done (to avoid multiple requests)
    // TODO handle onbeforeunload to detect leaving from the page with unsaved data
    useEffect(() => {
        updateData(tableComputationsService.serialize());
    }, [debouncedComputedTable, updateData, tableComputationsService]);

    const onCellValueEdit: OnCellValueEdit = useCallback(tableComputationsService.onCellValueEdit, [
        tableComputationsService,
    ]);
    const onColumnRename: OnColumnRename = useCallback(tableComputationsService.onColumnRename, [
        tableComputationsService,
    ]);
    const onAddNewRow: OnAddNewRow = useCallback(tableComputationsService.onAddNewRow, [
        tableComputationsService,
    ]);
    const onDeleteRow: OnDeleteRow = useCallback(tableComputationsService.onDeleteRow, [
        tableComputationsService,
    ]);
    const onDeleteColumn: OnDeleteColumn = useCallback(tableComputationsService.onDeleteColumn, [
        tableComputationsService,
    ]);
    const onSubmitColumn: OnSubmitColumn = useCallback(tableComputationsService.onSubmitColumn, [
        tableComputationsService,
    ]);
    const onResetData: OnResetData = useCallback(() => {
        tableComputationsService.dangerouslyResetAllStoredData();
        dataStorageModule.dangerouslyResetAllStorageData();
    }, [tableComputationsService, dataStorageModule]);

    const contextValue = useMemo<TableDataContextState>(() => {
        return {
            isSaving,
            headline,
            grid,
            aggregations,
            valuesDimensions,
            aggregationsDimensions,
            onCellValueEdit,
            onColumnRename,
            onAddNewRow,
            onDeleteRow,
            onDeleteColumn,
            onSubmitColumn,
            onResetData,
        };
    }, [
        isSaving,
        headline,
        grid,
        aggregations,
        valuesDimensions,
        aggregationsDimensions,
        onCellValueEdit,
        onColumnRename,
        onAddNewRow,
        onDeleteRow,
        onDeleteColumn,
        onSubmitColumn,
        onResetData,
    ]);
    const internalContextValue = useMemo<InternalTableDataContextState>(() => {
        return {
            dataStorageModule,
            tableComputationsService,
        };
    }, [dataStorageModule, tableComputationsService]);
    return (
        <TableDataContext.Provider value={contextValue}>
            <InternalTableDataContext.Provider value={internalContextValue}>
                {children}
            </InternalTableDataContext.Provider>
        </TableDataContext.Provider>
    );
};

export const useTableData = () => useContext(TableDataContext);

export type OnValidateExpression = (expression: string) => string | true;
export const useEditColumnDescriptorValues = (
    columnIndex: number,
): [StartEditColumnConfig, OnValidateExpression] => {
    const internalContextValue = useContext(InternalTableDataContext);
    if (!internalContextValue) {
        return [
            {
                values: {
                    name: '',
                    dynamic: false,
                    expressionString: '',
                    aggregations: {},
                },
                availableVariables: [],
                availableFunctions: [],
                availableAggregations: [],
            },
            () => true,
        ];
    }

    const { tableComputationsService } = internalContextValue;
    const onValidateExpression: OnValidateExpression = useCallback(
        (expression: string) => {
            return tableComputationsService.validateExpression(expression, columnIndex);
        },
        [tableComputationsService, columnIndex],
    );

    return useMemo((): [StartEditColumnConfig, OnValidateExpression] => {
        return [tableComputationsService.getEditColumnConfig(columnIndex), onValidateExpression];
    }, [tableComputationsService, columnIndex, onValidateExpression]);
};
