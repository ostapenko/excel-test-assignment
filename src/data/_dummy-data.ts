import type { IStorageData } from '../modules/data-storage/types';
import { IColumnType } from '../modules/data-storage/types';

export const _dummyTableData: IStorageData = {
    columns: [
        { name: 'Time', type: IColumnType.plain, expression: '', aggregations: {} },
        {
            name: 'Cell Density',
            type: IColumnType.plain,
            expression: '',
            aggregations: { MEAN: true, STD: true },
        },
        {
            name: 'Volume',
            type: IColumnType.plain,
            expression: '',
            aggregations: { MEAN: true, STD: true },
        },
        {
            name: 'Total Cells',
            type: IColumnType.dynamic,
            expression: 'B * C',
            aggregations: { SUM: true, MAX: true, MIN: true },
        },
        { name: 'Max Population', type: IColumnType.plain, expression: '', aggregations: {} },
        {
            name: 'Volume Usage',
            type: IColumnType.dynamic,
            expression: 'PERCENT((B * C) / E, 3)',
            aggregations: {},
        },
        {
            name: 'Cells Reached 150k',
            type: IColumnType.dynamic,
            expression: 'IF((D - 150000) > 0, "SUCCESS", "FAILURE")',
            aggregations: {},
        },
        {
            name: 'Example column',
            type: IColumnType.dynamic,
            expression: 'ABS(B - 151)',
            aggregations: { SUM: true, PRODUCT: true, STD: true, MEAN: true },
        },
    ],
    values: [
        [
            '2021-01-01T20:39:26.023Z',
            '2021-01-02T20:39:26.023Z',
            '2021-01-03T20:39:26.023Z',
            '2021-01-04T20:39:26.023Z',
            '2021-01-05T20:39:26.023Z',
            '2021-01-06T20:39:26.023Z',
        ],
        [100, 120, 140, 150, 166, 174],
        [990, 980, 970, 960, 956, 954],
        [],
        [150000, 150000, 150000, 150000, 200000, 200000],
        [],
        [],
        [],
    ],
};
