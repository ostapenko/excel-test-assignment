import isNumber from 'is-number';
import difference from 'lodash/difference';
import uniq from 'lodash/uniq';
import { all, create, EvalFunction, MathNode } from 'mathjs';
import memoizeLatest from 'memoize-one';

import { aggregationFunctions, equationFunctions } from './supported-functions';

const mathjs = create(all);
const parse = mathjs.parse;

export interface EEExpression {
    original: string;
    normalized: string;

    variables: string[];
    functions: string[];
    unknownVariables: string[];
    unknownFunctions: string[];

    parsedTree: MathNode;
    compiled: () => EvalFunction;
}

export default class EquationsEngine {
    readonly availableFunctions: string[];
    readonly availableAggregations: string[];

    constructor() {
        mathjs.import(equationFunctions, { wrap: true, silent: true });
        mathjs.import(aggregationFunctions, { wrap: true, silent: true });
        mathjs.import(
            {
                import: function () {
                    throw new Error('Function import is disabled');
                },
                createUnit: function () {
                    throw new Error('Function createUnit is disabled');
                },
                evaluate: function () {
                    throw new Error('Function evaluate is disabled');
                },
                parse: function () {
                    throw new Error('Function parse is disabled');
                },
                simplify: function () {
                    throw new Error('Function simplify is disabled');
                },
                derivative: function () {
                    throw new Error('Function derivative is disabled');
                },
            },
            { override: true },
        );

        this.availableFunctions = uniq(Object.keys(equationFunctions));
        this.availableAggregations = uniq(Object.keys(aggregationFunctions));
    }

    static GetAliasForColumnIndex(columnIndex: number): string {
        const letterACode = 'A'.charCodeAt(0);
        return String.fromCharCode(letterACode + columnIndex);
    }

    static ParseValue(value: string): string | number {
        if (isNumber(value)) {
            return mathjs.number(value);
        }
        return value.trim();
    }

    parseExpression(
        expression: string,
        allowedVariables: string[],
        allowedFunctions: string[],
    ): EEExpression {
        const originalExpression = expression.toUpperCase();
        const tree = parse(originalExpression);
        const usedVariables: string[] = [];
        const usedFunctions: string[] = [];
        tree.traverse((node, _, parentNode) => {
            const n = node as any;
            switch (node.type) {
                case 'FunctionNode':
                    usedFunctions.push(n.fn.name);
                    break;
                case 'SymbolNode':
                    if (parentNode.type !== 'FunctionNode') {
                        usedVariables.push(n.name);
                    }
                    break;
                default:
                    break;
            }
        });

        const variables = uniq(usedVariables);
        const functions = uniq(usedFunctions);
        const parsedTree = tree.cloneDeep();
        const compile = memoizeLatest((node: MathNode) => {
            return node.compile();
        });
        return {
            original: originalExpression,
            normalized: tree.toString({ implicit: 'show', parenthesis: 'keep' }),
            parsedTree,
            compiled: () => compile(parsedTree),
            variables,
            functions,
            unknownVariables: difference(usedVariables, allowedVariables),
            unknownFunctions: difference(usedFunctions, allowedFunctions),
        };
    }

    evaluateExpression(expr: EEExpression, values: any[]): any {
        const scope: Record<string, any> = {};
        for (let index = 0; index < values.length; index++) {
            scope[EquationsEngine.GetAliasForColumnIndex(index)] = values[index];
        }
        return expr.compiled().evaluate(scope);
    }

    calculateAggregateValue(funcCode: string, columnValues: any[]): any {
        if (funcCode in aggregationFunctions) {
            return aggregationFunctions[funcCode].call(null, columnValues);
        }

        throw new Error(`Function ${funcCode} is not supported`);
    }

    public getExpressionConstrainsForColumn(columnIndex: number): ExpressionConstrains {
        const availableVariables: string[] = [];
        for (let index = 0; index < columnIndex; index++) {
            availableVariables.push(EquationsEngine.GetAliasForColumnIndex(index));
        }
        return {
            availableFunctions: this.availableFunctions.slice(0),
            availableAggregations: this.availableAggregations.slice(0),
            availableVariables,
        };
    }

    public validateExpression(expression: string, columnIndex: number): string | true {
        const availableVariables: string[] = [];
        for (let index = 0; index < columnIndex; index++) {
            availableVariables.push(EquationsEngine.GetAliasForColumnIndex(index));
        }

        try {
            const eeExpression = this.parseExpression(
                expression,
                availableVariables,
                this.availableFunctions,
            );

            const invalidSymbols = [
                ...eeExpression.unknownFunctions,
                ...eeExpression.unknownVariables,
            ];
            if (invalidSymbols.length) {
                return `Invalid names used: ${invalidSymbols.join(', ')}`;
            }
        } catch (e) {
            if (e instanceof SyntaxError) {
                return e.message;
            }
            return 'Invalid expression';
        }

        return true;
    }
}

interface ExpressionConstrains {
    availableVariables: string[];
    availableFunctions: string[];
    availableAggregations: string[];
}
