import * as mathjs from 'mathjs';

const percent = (value: number, fractionDigits?: number): string => {
    const percentageValue = value * 100;
    const stringRepresentation =
        typeof fractionDigits === 'number'
            ? percentageValue.toFixed(fractionDigits)
            : percentageValue.toString(10);
    return `${stringRepresentation}%`;
};

const ifFunction = (condition: boolean, valueIfTrue: string, valueIfFalse: string): string => {
    return condition ? String(valueIfTrue) : String(valueIfFalse);
};

type MathFunction = (...args: any[]) => any;

export const equationFunctions: Record<string, MathFunction> = {
    PERCENT: percent,
    IF: ifFunction,
    // eslint-disable-next-line @typescript-eslint/unbound-method
    ABS: mathjs.abs,
    // eslint-disable-next-line @typescript-eslint/unbound-method
    MAX: mathjs.max,
    // eslint-disable-next-line @typescript-eslint/unbound-method
    MIN: mathjs.min,
    // eslint-disable-next-line @typescript-eslint/unbound-method
    SIN: mathjs.sin,
    // eslint-disable-next-line @typescript-eslint/unbound-method
    COS: mathjs.cos,
    // eslint-disable-next-line @typescript-eslint/unbound-method
    SQRT: mathjs.sqrt,
};

export const aggregationFunctions: Record<string, MathFunction> = {
    // eslint-disable-next-line @typescript-eslint/unbound-method
    SUM: mathjs.sum,
    // eslint-disable-next-line @typescript-eslint/unbound-method
    MEAN: mathjs.mean,
    // eslint-disable-next-line @typescript-eslint/unbound-method
    STD: mathjs.std,
    // eslint-disable-next-line @typescript-eslint/unbound-method
    MAX: mathjs.max,
    // eslint-disable-next-line @typescript-eslint/unbound-method
    MIN: mathjs.min,
    // eslint-disable-next-line @typescript-eslint/unbound-method
    PRODUCT: mathjs.prod,
};
