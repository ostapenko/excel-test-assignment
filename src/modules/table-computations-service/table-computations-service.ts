import isEqual from 'lodash/isEqual';
import memoizeLatest from 'memoize-one';

import { wait } from '../../utils/wait';
import { IColumnDescriptor, IColumnType, IPlainValue, IStorageData } from '../data-storage/types';
import EquationsEngine from '../equations-engine/equations-engine';
import {
    CellValue,
    ColumnAggregation,
    ColumnHeader,
    ColumnStoredDescriptor,
    ColumnValues,
    ComputedTable,
    DisplayDimensions,
    StartEditColumnConfig,
    TableValues,
} from './types';

const isCellValue = (maybeCellValue: unknown): maybeCellValue is CellValue => {
    return (
        typeof maybeCellValue === 'object' &&
        maybeCellValue != null &&
        'value' in maybeCellValue &&
        'invalidated' in maybeCellValue
    );
};

const createDefaultCellValue = (): CellValue => {
    return {
        value: '',
        invalidated: false,
        error: null,
    };
};

// TODO unit tests
const getGridDimensions = (grid: unknown[][]): DisplayDimensions => {
    return {
        cols: grid.length,
        rows: grid.reduce<number>((count, gridColumn) => Math.max(count, gridColumn.length), 0),
    };
};

export default class TableComputationsService {
    private listeners: Array<() => void> = [];

    private grid: TableValues = [];
    private aggregations: Array<ColumnAggregation[]> = [];
    private columnDescriptors: ColumnStoredDescriptor[] = [];

    private createSnapshot = memoizeLatest(
        (
            columnDescriptors: ColumnStoredDescriptor[],
            grid: TableValues,
            aggregations: Array<ColumnAggregation[]>,
        ): ComputedTable => ({
            headline: columnDescriptors.map<ColumnHeader>(descriptor => ({
                name: descriptor.name,
                alias: descriptor.alias,
                type: descriptor.type,
                serializedExpression: descriptor.serializedExpression,
                error: descriptor.error,
                invalidated: descriptor.invalidated,
            })),
            grid,
            aggregations,

            valuesDimensions: getGridDimensions(grid),
            aggregationsDimensions: getGridDimensions(aggregations),
        }),
    );
    private columnIndexesToInvalidate: Set<number> = new Set();
    private rowIndexesToInvalidate: Set<number> = new Set();
    private notificationScheduled = false;
    private calculationCycleIndex = 0;
    private calculationCycleSymbol: symbol | null = null;
    private calculation: Promise<true> = Promise.resolve(true);

    constructor(private readonly equationsEngine: EquationsEngine) {}

    public update({ columns, values }: IStorageData): void {
        if (columns.length < this.columnDescriptors.length) {
            for (let index = this.columnDescriptors.length - 1; index >= columns.length; index--) {
                this.dropColumn(index);
            }
        }
        const { rows: incomingGridRowsCount } = getGridDimensions(values);
        for (let index = 0; index < columns.length; index++) {
            this.updateColumnDescriptor(columns[index], index);

            this.updateGridValuesForColumn(index, incomingGridRowsCount, values[index]);
        }

        this.invalidateValues();
        this.notify();
    }

    public subscribe = (listener: () => void): (() => void) => {
        this.listeners = [...this.listeners, listener];
        return () => {
            this.listeners = this.listeners.filter(l => l !== listener);
        };
    };

    public getSnapshot = (): ComputedTable => {
        return this.createSnapshot(this.columnDescriptors, this.grid, this.aggregations);
    };

    public serialize(): IStorageData {
        return {
            values: this.grid.map(columnValues => {
                return columnValues.map(cellValue => cellValue.value);
            }),
            columns: this.columnDescriptors.map((descriptor): IColumnDescriptor => {
                return {
                    aggregations: { ...descriptor.aggregations },
                    expression: descriptor.serializedExpression,
                    name: descriptor.name,
                    type: descriptor.type,
                };
            }),
        };
    }

    public onColumnRename = (value: string, columnIndex: number): void => {
        const dimensions = getGridDimensions(this.grid);

        if (!(columnIndex < dimensions.cols)) {
            // TODO log error, because this should never happen under correct circumstances
            return;
        }

        const updatedName = value.trim();
        const descriptor = this.columnDescriptors[columnIndex];
        if (descriptor.name !== updatedName) {
            this.columnDescriptors[columnIndex] = {
                ...this.columnDescriptors[columnIndex],
                name: updatedName,
            };
            this.scheduleNotification();
            this.columnDescriptors = this.columnDescriptors.slice(0);
        }

        this.notify();
    };

    public getEditColumnConfig(columnIndex: number): StartEditColumnConfig {
        const dimensions = getGridDimensions(this.grid);

        const { availableFunctions, availableAggregations, availableVariables } =
            this.equationsEngine.getExpressionConstrainsForColumn(columnIndex);
        const values: StartEditColumnConfig['values'] = {
            name: '',
            dynamic: false,
            expressionString: '',
            aggregations: {},
        };
        if (columnIndex < dimensions.cols) {
            const descriptor = this.columnDescriptors[columnIndex];

            values.name = descriptor.name;
            values.expressionString = descriptor.serializedExpression;
            values.dynamic = descriptor.type === IColumnType.dynamic;
            values.aggregations = { ...descriptor.aggregations };
        }

        return {
            values,
            availableVariables,
            availableFunctions,
            availableAggregations,
        };
    }

    public validateExpression(expression: string, columnIndex: number): string | true {
        return this.equationsEngine.validateExpression(expression, columnIndex);
    }

    public onAddNewRow = (): void => {
        this.grid = this.grid.map((columnValues, columnIndex) => {
            this.setColumnForInvalidation({ columnIndex });
            return [...columnValues, createDefaultCellValue()];
        });

        const { rows } = getGridDimensions(this.grid);

        this.setRowForInvalidation({ rowIndex: rows - 1 });
        this.scheduleNotification();
        this.invalidateValues();
        this.notify();
    };

    public onDeleteRow = (rowIndex: number): void => {
        const dimensions = getGridDimensions(this.grid);

        if (!(rowIndex < dimensions.rows)) {
            // TODO log error, because this should never happen under correct circumstances
            return;
        }

        this.grid = this.grid.map((columnValues, columnIndex) => {
            columnValues.splice(rowIndex, 1);
            this.setColumnForInvalidation({ columnIndex });
            return [...columnValues];
        });

        this.scheduleNotification();
        this.invalidateValues();
        this.notify();
    };

    public onSubmitColumn = (
        values: StartEditColumnConfig['values'],
        columnIndex: number,
    ): void => {
        const dimensions = getGridDimensions(this.grid);

        if (columnIndex < dimensions.cols) {
            // TODO implement column editing later
            return;
        }

        columnIndex = dimensions.cols;
        this.updateColumnDescriptor(
            {
                aggregations: Object.keys(values.aggregations).reduce<Record<string, true>>(
                    (all, code) => {
                        if (values.aggregations[code]) {
                            all[code] = true;
                        }
                        return all;
                    },
                    {},
                ),
                expression: values.dynamic ? values.expressionString : '',
                name: values.name,
                type: values.dynamic ? IColumnType.dynamic : IColumnType.plain,
            },
            columnIndex,
        );
        this.updateGridValuesForColumn(columnIndex, dimensions.rows, null);

        this.invalidateValues();
        this.notify();
    };

    public onDeleteColumn = (columnIndex: number): void => {
        const dimensions = getGridDimensions(this.grid);

        if (columnIndex !== dimensions.cols - 1) {
            // TODO log error, because this should never happen under correct circumstances
            return;
        }

        this.dropColumn(columnIndex);

        this.scheduleNotification();
        this.notify();
    };

    public onCellValueEdit = (
        value: string,
        place: { rowIndex: number; columnIndex: number },
    ): void => {
        const dimensions = getGridDimensions(this.grid);
        const { rowIndex, columnIndex } = place;
        if (!(rowIndex < dimensions.rows && columnIndex < dimensions.cols)) {
            // TODO log error, because this should never happen under correct circumstances
            return;
        }

        const valueToSet = EquationsEngine.ParseValue(value);

        const descriptor = this.columnDescriptors[columnIndex];
        switch (descriptor.type) {
            case IColumnType.dynamic: {
                // we shouldn't be able to enter this branch, calculated columns aren't editable
                // TODO log error, because this should never happen under correct circumstances
                break;
            }
            case IColumnType.plain: {
                const storedValue = this.grid[columnIndex][rowIndex];
                if (storedValue.value !== valueToSet) {
                    this.grid[columnIndex][rowIndex] = {
                        ...storedValue,
                        value: valueToSet,
                    };
                    this.grid[columnIndex] = this.grid[columnIndex].slice(0);

                    this.setColumnForInvalidation({ columnIndex });
                    this.setRowForInvalidation({ rowIndex });
                }
                break;
            }
            default: {
                // eslint-disable-next-line @typescript-eslint/no-unused-vars
                const _: never = descriptor.type;
            }
        }

        this.invalidateValues();
        this.notify();
    };

    // use only for debugging
    public dangerouslyResetAllStoredData = (): void => {
        this.grid = [];
        this.aggregations = [];
        this.columnDescriptors = [];
        this.calculationCycleIndex++;
        this.calculationCycleSymbol = Symbol(`№${this.calculationCycleIndex}`);
        this.calculation = Promise.resolve(true);
        this.notify();
    };

    private calculate(): void {
        this.calculationCycleIndex++;
        this.calculationCycleSymbol = Symbol(`№${this.calculationCycleIndex}`);

        const { rows: rowsCount, cols: columnsCount } = getGridDimensions(this.grid);
        for (let col = 0; col < columnsCount; col++) {
            for (let row = 0; row < rowsCount; row++) {
                if (this.grid[col][row].invalidated) {
                    this.calculation = this.calculation.then(
                        this.addCellCalculationMicroTask(this.calculationCycleSymbol, { col, row }),
                    );
                }
            }
        }

        this.calculation = this.calculation.then(
            this.addAllAggregationsCalculationMicroTask(this.calculationCycleSymbol),
        );

        this.calculation = this.calculation.then(
            this.addMarkColumnsReadyMicroTask(this.calculationCycleSymbol),
        );
    }

    private updateColumnDescriptor(incomingDescriptor: IColumnDescriptor, columnIndex: number) {
        let descriptor = this.columnDescriptors[columnIndex] || {
            name: '',
            alias: '',

            type: '' as IColumnType, // must be set to a valid value later
            serializedExpression: '',

            eeExpression: null,
            invalidated: false,
            error: null,
            aggregations: {},
        };
        let aggregationsArray = this.aggregations[columnIndex] || [];

        if (descriptor.name !== incomingDescriptor.name) {
            descriptor = {
                ...descriptor,
                name: incomingDescriptor.name,
            };
            this.scheduleNotification();
        }

        const columnAlias = EquationsEngine.GetAliasForColumnIndex(columnIndex);
        if (descriptor.alias !== columnAlias) {
            descriptor = {
                ...descriptor,
                alias: columnAlias,
            };
            this.scheduleNotification();
        }

        if (!isEqual(descriptor.aggregations, incomingDescriptor.aggregations)) {
            descriptor = {
                ...descriptor,
                aggregations: { ...incomingDescriptor.aggregations },
            };
            this.setColumnForInvalidation({ columnIndex });
            this.scheduleNotification();
        }

        const storedAggregations = aggregationsArray.reduce<Record<string, true>>((hash, entry) => {
            hash[entry.name] = true;
            return hash;
        }, {});
        if (!isEqual(descriptor.aggregations, storedAggregations)) {
            aggregationsArray = Object.keys(descriptor.aggregations).map<ColumnAggregation>(
                aggregationFuncCode => {
                    return {
                        value: '',
                        funcCode: aggregationFuncCode,
                        name: aggregationFuncCode, // TODO fill in from the config
                        description: '', // TODO fill in from the config

                        invalidated: true,
                        error: null,
                    };
                },
            );
            this.setColumnForInvalidation({ columnIndex });
            this.scheduleNotification();
        }

        if (descriptor.type !== incomingDescriptor.type) {
            descriptor = {
                ...descriptor,
                type: incomingDescriptor.type,
            };
            this.setColumnForInvalidation({ columnIndex });
            this.scheduleNotification();
        }

        switch (descriptor.type) {
            case IColumnType.dynamic: {
                if (descriptor.serializedExpression !== incomingDescriptor.expression) {
                    descriptor = {
                        ...descriptor,
                        serializedExpression: incomingDescriptor.expression,
                        error: null,
                        eeExpression: null,
                        invalidated: true,
                    };
                    try {
                        const eeExpression = this.equationsEngine.parseExpression(
                            incomingDescriptor.expression,
                            [],
                            [],
                        );
                        descriptor.serializedExpression = eeExpression.normalized;
                        descriptor.eeExpression = eeExpression;
                    } catch {
                        // TODO handle errors
                    }
                    this.setColumnForInvalidation({ columnIndex });
                    this.scheduleNotification();
                }
                break;
            }
            case IColumnType.plain: {
                descriptor = {
                    ...descriptor,
                    serializedExpression: '',
                    error: null,
                    eeExpression: null,
                    invalidated: false,
                };
                this.scheduleNotification();
                break;
            }
            default: {
                // eslint-disable-next-line @typescript-eslint/no-unused-vars
                const _: never = descriptor.type;
            }
        }

        this.columnDescriptors[columnIndex] = descriptor;
        this.aggregations[columnIndex] = aggregationsArray;
    }

    private dropColumn(columnIndex: number) {
        if (columnIndex !== this.columnDescriptors.length - 1) {
            throw new Error('only the last column can be deleted');
        }

        this.columnDescriptors = this.columnDescriptors.slice(0, columnIndex);
        this.aggregations = this.aggregations.slice(0, columnIndex);
        this.grid = this.grid.slice(0, columnIndex);

        this.scheduleNotification();
    }

    private setColumnForInvalidation({ columnIndex }: { columnIndex: number }): void {
        this.columnIndexesToInvalidate.add(columnIndex);
    }

    private setRowForInvalidation({ rowIndex }: { rowIndex: number }): void {
        this.rowIndexesToInvalidate.add(rowIndex);
    }

    private invalidateValues(): void {
        // it's important to invalidate all rows first since it can cause invalidations in the columns that depend on the changed cell
        // TODO save cell coordinates and invalidate only the columns that use this cell specifically
        // right now we invalidate all dynamic columns, which is not the best
        for (let columnIndex = 0; columnIndex < this.columnDescriptors.length; columnIndex++) {
            const descriptor = this.columnDescriptors[columnIndex];
            switch (descriptor.type) {
                case IColumnType.dynamic: {
                    this.rowIndexesToInvalidate.forEach(rowIndex => {
                        if (this.aggregations[columnIndex].length > 0) {
                            this.columnDescriptors[columnIndex] = {
                                ...this.columnDescriptors[columnIndex],
                                invalidated: true,
                            };

                            this.aggregations[columnIndex] = this.aggregations[columnIndex].map(
                                aggregationValue => {
                                    return {
                                        ...aggregationValue,
                                        invalidated: true,
                                    };
                                },
                            );
                        }

                        this.grid[columnIndex][rowIndex] = {
                            ...this.grid[columnIndex][rowIndex],
                            invalidated: true,
                        };
                    });

                    break;
                }
                case IColumnType.plain: {
                    // for plain values columns invalidation affects aggregations only
                    break;
                }
                default: {
                    // eslint-disable-next-line @typescript-eslint/no-unused-vars
                    const _: never = descriptor.type;
                }
            }
        }

        this.columnIndexesToInvalidate.forEach(columnIndex => {
            if (this.aggregations[columnIndex].length > 0) {
                this.columnDescriptors[columnIndex] = {
                    ...this.columnDescriptors[columnIndex],
                    invalidated: true,
                };

                this.aggregations[columnIndex] = this.aggregations[columnIndex].map(
                    aggregationValue => {
                        return {
                            ...aggregationValue,
                            invalidated: true,
                        };
                    },
                );
            }

            const descriptor = this.columnDescriptors[columnIndex];

            switch (descriptor.type) {
                case IColumnType.dynamic: {
                    this.grid[columnIndex] = this.grid[columnIndex].map(cellValue => {
                        return {
                            ...cellValue,
                            invalidated: true,
                        };
                    });
                    break;
                }
                case IColumnType.plain: {
                    // for plain values columns invalidation affects aggregations only
                    break;
                }
                default: {
                    // eslint-disable-next-line @typescript-eslint/no-unused-vars
                    const _: never = descriptor.type;
                }
            }
        });

        if (this.rowIndexesToInvalidate.size > 0 || this.columnIndexesToInvalidate.size > 0) {
            this.columnDescriptors = this.columnDescriptors.slice(0);
            this.aggregations = this.aggregations.slice(0);
            this.grid = this.grid.slice(0);

            this.scheduleNotification();
        }

        this.rowIndexesToInvalidate.clear();
        this.columnIndexesToInvalidate.clear();
        this.calculate();
    }

    private updateGridValuesForColumn(
        columnIndex: number,
        gridRowsCount: number,
        values: Array<IPlainValue | null> | null,
    ) {
        const descriptor = this.columnDescriptors[columnIndex];

        let gridColumn: ColumnValues = this.grid[columnIndex] ?? null;
        if (!gridColumn) {
            gridColumn = [];
            this.setColumnForInvalidation({ columnIndex });
            this.scheduleNotification();
        }

        for (let rowIndex = 0; rowIndex < gridRowsCount; rowIndex++) {
            if (!isCellValue(gridColumn[rowIndex])) {
                gridColumn[rowIndex] = createDefaultCellValue();
                this.setColumnForInvalidation({ columnIndex });
                this.setRowForInvalidation({ rowIndex });
                this.scheduleNotification();
            }

            switch (descriptor.type) {
                case IColumnType.dynamic: {
                    // the column is calculated, we only need to ensure the size and the shape
                    // we should ignore the incoming values
                    // doing nothing
                    break;
                }
                case IColumnType.plain: {
                    const incomingValue = values?.[rowIndex];
                    if (typeof incomingValue !== 'undefined' && typeof incomingValue !== 'object') {
                        if (gridColumn[rowIndex].value !== incomingValue) {
                            // incoming value differs from the stored value
                            gridColumn[rowIndex] = {
                                value: incomingValue,
                                invalidated: false,
                                error: null,
                            };

                            this.setColumnForInvalidation({ columnIndex });
                            this.setRowForInvalidation({ rowIndex });
                            this.scheduleNotification();
                        }

                        break;
                    }
                    break;
                }
                default: {
                    // eslint-disable-next-line @typescript-eslint/no-unused-vars
                    const _: never = descriptor.type;
                }
            }
        }

        this.grid[columnIndex] = gridColumn;
    }

    private scheduleNotification() {
        this.notificationScheduled = true;
    }

    private notify(): void {
        if (!this.notificationScheduled) {
            return;
        }
        for (const listener of this.listeners) {
            try {
                listener();
            } catch {
                // ignore
            }
        }
        this.notificationScheduled = false;
    }

    private addCellCalculationMicroTask(
        calculationCycleSymbol: symbol,
        {
            col,
            row,
        }: {
            col: number;
            row: number;
        },
    ): () => Promise<true> {
        // use async function here to provide room for flexibility in the future
        // for example, we could move computations to workers or backend later, should we require to
        // eslint-disable-next-line @typescript-eslint/require-await
        return async () => {
            // TODO create a helper wrapping function that'll check the symbol in one place,
            // instead of trying to remember adding this check to all add*MicroTask functions
            if (this.calculationCycleSymbol !== calculationCycleSymbol) {
                return true;
            }
            await wait(0.01); // for visual effect and to demonstrate how it would look like with async computations

            const cellValue = this.grid[col][row];
            const descriptor = this.columnDescriptors[col];
            const eeExpression =
                descriptor.eeExpression ||
                this.equationsEngine.parseExpression(descriptor.serializedExpression, [], []);
            descriptor.eeExpression = eeExpression;

            const result = this.equationsEngine.evaluateExpression(
                eeExpression,
                this.grid.map(column => column[row].value),
            );

            if (descriptor.eeExpression !== eeExpression) {
                descriptor.eeExpression = eeExpression;
                this.scheduleNotification();
            }

            this.grid[col][row] = {
                ...cellValue,
                value: result,
                invalidated: false,
                error: null,
            };

            this.grid[col] = this.grid[col].slice(0);
            this.grid = this.grid.slice(0);
            this.scheduleNotification();

            this.notify();
            return true;
        };
    }

    private addMarkColumnsReadyMicroTask(calculationCycleSymbol: symbol): () => Promise<true> {
        // use async function here to provide room for flexibility in the future
        // for example, we could move computations to workers or backend later, should we require to
        // eslint-disable-next-line @typescript-eslint/require-await
        return async () => {
            // TODO create a helper wrapping function that'll check the symbol in one place,
            // instead of trying to remember adding this check to all add*MicroTask functions
            if (this.calculationCycleSymbol !== calculationCycleSymbol) {
                return true;
            }

            let hasChanged = false;
            for (let columnIndex = 0; columnIndex < this.columnDescriptors.length; columnIndex++) {
                const cellsSettled = this.grid[columnIndex].every(cell => !cell.invalidated);
                const aggregationsSettled = this.aggregations[columnIndex].every(
                    aggregation => !aggregation.invalidated,
                );
                if (cellsSettled && aggregationsSettled) {
                    if (this.columnDescriptors[columnIndex].invalidated) {
                        hasChanged = true;
                        this.columnDescriptors[columnIndex] = {
                            ...this.columnDescriptors[columnIndex],
                            invalidated: false,
                            error: null,
                        };
                    }
                } else {
                    hasChanged = true;
                    this.columnDescriptors[columnIndex] = {
                        ...this.columnDescriptors[columnIndex],
                        // this shouldn't normally happen
                        // TODO to future proof we could schedule recalculations of certain values and/or propagate errors etc
                        error: new Error(
                            'cells or aggregations are not calculated, (broken state)',
                        ),
                        invalidated: false,
                    };
                }
            }

            if (hasChanged) {
                this.columnDescriptors = this.columnDescriptors.slice(0);
                this.scheduleNotification();
            }

            this.notify();
            return true;
        };
    }

    private addAllAggregationsCalculationMicroTask(
        calculationCycleSymbol: symbol,
    ): () => Promise<true> {
        // use async function here to provide room for flexibility in the future
        // for example, we could move computations to workers or backend later, should we require to
        // eslint-disable-next-line @typescript-eslint/require-await
        return async () => {
            // TODO create a helper wrapping function that'll check the symbol in one place,
            // instead of trying to remember adding this check to all add*MicroTask functions
            if (this.calculationCycleSymbol !== calculationCycleSymbol) {
                return true;
            }

            await Promise.all(
                this.aggregations
                    .flatMap((aggregationsArray, columnIndex) => {
                        return aggregationsArray.map((aggregation, arrayIndex) => {
                            return {
                                aggregation,
                                columnIndex,
                                arrayIndex,
                            };
                        });
                    })
                    // eslint-disable-next-line @typescript-eslint/require-await
                    .map(async ({ aggregation, columnIndex, arrayIndex }): Promise<true> => {
                        await wait(0.01);
                        if (this.calculationCycleSymbol !== calculationCycleSymbol) {
                            return true;
                        }
                        const aggregateValue = this.equationsEngine.calculateAggregateValue(
                            aggregation.funcCode,
                            this.grid[columnIndex].map(cell => cell.value),
                        );
                        this.aggregations[columnIndex][arrayIndex] = {
                            ...aggregation,
                            value: aggregateValue,
                            invalidated: false,
                            error: null,
                        };
                        this.aggregations[columnIndex] = this.aggregations[columnIndex].slice(0);
                        this.aggregations = this.aggregations.slice(0);

                        this.scheduleNotification();
                        this.notify();

                        return true;
                    }),
            );

            this.notify();
            return true;
        };
    }
}
