import { IColumnType } from '../data-storage/types';
import { EEExpression } from '../equations-engine/equations-engine';

type DefaultCellValueType = string | number;
export interface CellValue<T = DefaultCellValueType> {
    value: T;

    invalidated: boolean;
    error: Error | null;
}

export type ColumnValues<T = DefaultCellValueType> = Array<CellValue<T>>;

export type TableValues = Array<ColumnValues>;

export type DisplayDimensions = { cols: number; rows: number };

export interface ColumnStoredDescriptor {
    name: string;
    alias: string;

    type: IColumnType;
    serializedExpression: string;

    eeExpression: EEExpression | null;
    invalidated: boolean;
    error: Error | null;
    aggregations: Record<string, true>;
}

export interface ColumnHeader {
    name: string;
    alias: string;
    type: IColumnType;
    serializedExpression: string;

    invalidated: boolean;
    error: Error | null;
}

export interface ColumnAggregation<T = DefaultCellValueType> {
    value: T;
    funcCode: string;
    name: string;
    description: string;

    invalidated: boolean;
    error: Error | null;
}

export interface ComputedTable {
    headline: ColumnHeader[];
    grid: TableValues;
    aggregations: Array<ColumnAggregation[]>;

    valuesDimensions: DisplayDimensions;
    aggregationsDimensions: DisplayDimensions;
}

export interface StartEditColumnConfig {
    values: {
        name: string;
        dynamic: boolean;
        expressionString: string;
        aggregations: Record<string, boolean>;
    };
    availableVariables: string[];
    availableFunctions: string[];
    availableAggregations: string[];
}
