export type IPlainValue = number | string;

export enum IColumnType {
    plain = 'plain',
    dynamic = 'dynamic',
}
export interface IColumnDescriptor {
    name: string;
    type: IColumnType;
    expression: string;
    aggregations: Record<string, true>;
}
export type IStorageData = {
    columns: Array<IColumnDescriptor>;
    values: Array<Array<IPlainValue>>;
};
export type IStorageRecord = {
    data: IStorageData;
    version: number;
};
