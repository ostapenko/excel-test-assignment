import 'localforage';

import isEqual from 'lodash/isEqual';
import memoizeLatest from 'memoize-one';

import { wait } from '../../utils/wait';
import type { IStorageData, IStorageRecord } from './types';

const DATA_SCHEMA_VERSION = 2.0;

function createTableRecord(tableData: IStorageData): IStorageRecord {
    return {
        data: tableData,
        version: DATA_SCHEMA_VERSION,
    };
}

function isTableRecord(tableRecord: unknown): tableRecord is IStorageRecord {
    return (
        typeof tableRecord === 'object' &&
        tableRecord != null &&
        'data' in tableRecord &&
        'version' in tableRecord
    );
}

export default class DataStorageModule {
    static readonly EMPTY_TABLE_DATA: IStorageData = { columns: [], values: [] };

    private fetchDataPromise: Promise<true> | null = null;
    private saveDataPromise: Promise<true> | null = null;

    private latestData: IStorageData = DataStorageModule.EMPTY_TABLE_DATA;
    private initialFetchDone = false;

    private readonly ITEM_NAME = 'table_data';
    private readonly DATA_SCHEMA_VERSION = DATA_SCHEMA_VERSION;

    private listeners: Array<() => void> = [];
    private createSnapshot = memoizeLatest(
        (tableData: IStorageData, isSaving: boolean) => [tableData, isSaving] as const,
    );

    constructor(
        private readonly localforage: LocalForage,
        private readonly dummyTableData: IStorageData,
    ) {
        this.localforage.config({
            // setting LOCALSTORAGE only because it supports 'storage' event
            // in production it would be some external serverside storage with the support of change notifications
            driver: this.localforage.LOCALSTORAGE,
            name: 'opvia',
            version: this.DATA_SCHEMA_VERSION,
            storeName: 'opvia_table', // Should be alphanumeric, with underscores.
            description: 'Opvia Columns Calculator',
        });
    }

    public subscribe = (listener: () => void): (() => void) => {
        this.listeners = [...this.listeners, listener];
        return () => {
            this.listeners = this.listeners.filter(l => l !== listener);
        };
    };

    public getSnapshot = (): readonly [IStorageData, boolean] => {
        return this.createSnapshot(this.latestData, this.saveDataPromise !== null);
    };

    public updateData(data: IStorageData): Promise<true> {
        if (isEqual(data, this.latestData)) {
            // data is latest, no update is needed
            if (this.saveDataPromise) {
                // an update is in flight, just returning the promise
                return this.saveDataPromise;
            }
            return Promise.resolve(true);
        }

        this.latestData = data;

        this.saveDataPromise = Promise.resolve(this.saveDataPromise || true)
            .then(() => this.localforage.setItem(this.ITEM_NAME, createTableRecord(data)))
            .then(() => {
                // TODO for debugging, emulating slow network
                return wait(0.2);
            })
            .then(() => {
                return true as const;
            })
            .finally(() => {
                if (isEqual(data, this.latestData)) {
                    this.saveDataPromise = null;
                    this.notify();
                }
            });

        this.notify();
        return this.saveDataPromise;
    }

    public fetchData(): Promise<true> {
        if (this.fetchDataPromise) {
            return this.fetchDataPromise;
        }

        this.fetchDataPromise = this.localforage
            .getItem<IStorageRecord>(this.ITEM_NAME)
            .then(tableData => {
                if (isTableRecord(tableData)) {
                    if (tableData.version === this.DATA_SCHEMA_VERSION) {
                        return this.updateData(tableData.data);
                    }
                    // todo implement schema mutations based on the data version
                    void 0;
                }

                return this.localforage.clear().then(() => {
                    return this.updateData(this.dummyTableData);
                });
            })
            .then(() => {
                // TODO for debugging, emulating slow network
                return wait(0.4);
            })
            .then(() => {
                this.initialFetchDone = true;
                return true as const;
            })
            .finally(() => {
                this.fetchDataPromise = null;
            });

        return this.fetchDataPromise;
    }

    public isInitialFetchDone(): boolean {
        return this.initialFetchDone;
    }

    // use only for debugging
    public dangerouslyResetAllStorageData = (): void => {
        this.initialFetchDone = false;
        void this.localforage.clear();
        this.notify();
    };

    private notify(): void {
        for (const listener of this.listeners) {
            try {
                listener();
            } catch {
                // ignore
            }
        }
    }
}
