import React, { useCallback } from 'react';
import { Controller, useForm } from 'react-hook-form';

import {
    Button,
    Card,
    Checkbox,
    Dialog,
    DialogBody,
    DialogFooter,
    Elevation,
    FormGroup,
    InputGroup,
    Intent,
    Switch,
    TextArea,
} from '@blueprintjs/core';
import cn from 'classnames';

import { useEditColumnDescriptorValues } from '../../providers/table-data-provider/table-data-provider';
import styles from './styles.module.css';

interface AddColumnDialogProps {
    isOpen: boolean;
    onClose: () => void;
    columnIndex: number;
    onSubmit: (values: AddColumnForm, columnIndex: number) => void;
}

interface AddColumnForm {
    name: string;
    dynamic: boolean;
    expressionString: string;
    aggregations: Record<string, boolean>;
}

export const AddColumnDialog: React.FC<AddColumnDialogProps> = ({
    isOpen,
    onClose,
    columnIndex,
    onSubmit,
}: AddColumnDialogProps) => {
    const [
        { values, availableVariables, availableAggregations, availableFunctions },
        onValidateExpressionString,
    ] = useEditColumnDescriptorValues(columnIndex);

    const {
        handleSubmit,
        formState: { errors, dirtyFields },
        control,
    } = useForm<AddColumnForm>({
        defaultValues: values,
        reValidateMode: 'onSubmit',
    });

    const onFormSubmit = useCallback(
        (event: React.FormEvent<HTMLFormElement>) => {
            void handleSubmit(formValues => {
                onSubmit(formValues, columnIndex);
                onClose();
            })(event);
        },
        [handleSubmit, onSubmit, onClose, columnIndex],
    );

    return (
        <Dialog isOpen={isOpen} title="Add New Column" icon="settings" onClose={onClose}>
            <form onSubmit={onFormSubmit}>
                <DialogBody className={cn(styles.dialog)}>
                    <Controller
                        name="name"
                        control={control}
                        rules={{ required: true, minLength: 2, maxLength: 50 }}
                        render={({ field: { ref, ...rest }, formState }) => (
                            <FormGroup
                                disabled={formState.isLoading}
                                subLabel="Name length between 2 and 50"
                                label="Column name"
                                labelFor="name-input"
                                labelInfo="(required)"
                                intent={errors.name ? Intent.DANGER : Intent.NONE}
                            >
                                <InputGroup
                                    fill
                                    id="name-input"
                                    placeholder="start typing"
                                    intent={errors.name ? Intent.DANGER : Intent.NONE}
                                    inputRef={ref}
                                    {...rest}
                                />
                            </FormGroup>
                        )}
                    />
                    <Controller
                        name="dynamic"
                        control={control}
                        render={({ field: { ref, value, ...rest } }) => (
                            <Switch
                                inputRef={ref}
                                label="Dynamic column"
                                checked={!!value}
                                {...rest}
                            />
                        )}
                    />
                    <Controller
                        name="expressionString"
                        control={control}
                        rules={{
                            required: {
                                value: !!dirtyFields.dynamic,
                                message: 'Provide non-empty expression',
                            },
                            validate: onValidateExpressionString,
                        }}
                        render={({ field: { ref, ...rest }, formState }) => {
                            const disabledCard = !dirtyFields.dynamic || formState.isLoading;
                            const expressionsPlaceholder =
                                '(A + B) / C\nPERCENT(D / 10, 2)\nIF(A >= 0, "Positive", "Non-positive")\nABS(B - 151)';
                            return (
                                <Card elevation={Elevation.ONE}>
                                    <FormGroup
                                        disabled={disabledCard}
                                        label="Add expresion"
                                        labelFor="expression-input"
                                        intent={
                                            errors.expressionString ? Intent.DANGER : Intent.NONE
                                        }
                                        helperText={errors.expressionString?.message || ''}
                                    >
                                        <TextArea
                                            fill
                                            growVertically
                                            id="expression-input"
                                            placeholder={expressionsPlaceholder}
                                            intent={
                                                errors.expressionString
                                                    ? Intent.DANGER
                                                    : Intent.NONE
                                            }
                                            inputRef={ref}
                                            {...rest}
                                            disabled={disabledCard}
                                        />
                                    </FormGroup>
                                    <section
                                        className={cn('bp4-running-text', {
                                            'bp4-text-disabled': disabledCard,
                                        })}
                                    >
                                        <p>
                                            Please, provide an expression to calculate the values in
                                            this column.
                                        </p>
                                        <p>
                                            To use the values from the table, use letter codes
                                            assigned to each column in your expression. You may only
                                            reference the columns to the left of the current.
                                        </p>
                                        <p>
                                            Available variables include:{' '}
                                            {availableVariables.map((variable, index) => {
                                                const isLast =
                                                    index === availableVariables.length - 1;
                                                return (
                                                    <span key={variable}>
                                                        <code>{variable}</code>
                                                        {isLast ? '.' : ', '}
                                                    </span>
                                                );
                                            })}
                                        </p>
                                        <p>
                                            Available functions include:{' '}
                                            {availableFunctions.map((funcCode, index) => {
                                                const isLast =
                                                    index === availableFunctions.length - 1;
                                                return (
                                                    <span key={funcCode}>
                                                        <code>{funcCode}</code>
                                                        {isLast ? '.' : ', '}
                                                    </span>
                                                );
                                            })}
                                        </p>
                                        <p>
                                            You may also use parentheses as well as basic math and
                                            logical operations.
                                        </p>
                                    </section>
                                </Card>
                            );
                        }}
                    />
                    <h3>Please, select aggregate values to compute on this column</h3>
                    {availableAggregations.map(aggregationCode => {
                        return (
                            <Controller
                                key={aggregationCode}
                                name={`aggregations.${aggregationCode}`}
                                control={control}
                                render={({ field: { ref, value, ...rest } }) => (
                                    <Checkbox
                                        inputRef={ref}
                                        label={aggregationCode}
                                        checked={!!value}
                                        {...rest}
                                    />
                                )}
                            />
                        );
                    })}
                </DialogBody>
                <DialogFooter
                    actions={
                        <>
                            <Button intent={Intent.NONE} text="Cancel" onClick={onClose} minimal />
                            <Button
                                type="submit"
                                icon="saved"
                                intent={Intent.PRIMARY}
                                text="Submit"
                            />
                        </>
                    }
                />
            </form>
        </Dialog>
    );
};
