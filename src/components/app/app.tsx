import React, { useEffect, useState } from 'react';

import { Alignment, Button, Intent, Navbar, ProgressBar } from '@blueprintjs/core';
import cn from 'classnames';
import { useBoolean } from 'usehooks-ts';

import { useTableData } from '../../providers/table-data-provider/table-data-provider';
import { AddColumnDialog } from '../add-column-dialog/add-column-dialog';
import { OpviaTable } from '../opvia-table/opvia-table';
import styles from './styles.module.css';

export const App: React.FC = () => {
    const { isSaving, onAddNewRow, onResetData, onSubmitColumn, valuesDimensions } = useTableData();
    const {
        value: isDialogOpen,
        setTrue: onDialogOpen,
        setFalse: onDialogClose,
    } = useBoolean(false);
    const [progressBarValue, setProgressBarValue] = useState(0);

    useEffect(() => {
        let intervalId: number | null = null;
        if (isSaving) {
            setProgressBarValue(0.01);
            intervalId = window.setInterval(() => {
                setProgressBarValue(v => Math.min(0.6, v + 0.05));
            }, 33);
        }

        let timeoutId: number | null = null;
        if (!isSaving && progressBarValue > 0) {
            timeoutId = window.setTimeout(() => {
                setProgressBarValue(1);
                timeoutId = window.setTimeout(() => {
                    setProgressBarValue(0);
                }, 600);
            }, 300);
        }

        return () => {
            if (intervalId) {
                window.clearTimeout(intervalId);
            }
            if (timeoutId) {
                window.clearTimeout(timeoutId);
            }
        };
    }, [isSaving]);

    return (
        <div className={cn(styles.app)}>
            <Navbar>
                <Navbar.Group align={Alignment.LEFT}>
                    <Navbar.Heading>Opvia Columns Calculator</Navbar.Heading>
                    <Navbar.Divider />
                    <Button
                        intent={Intent.PRIMARY}
                        minimal
                        icon="add-row-bottom"
                        text="Add Row"
                        onClick={onAddNewRow}
                    />
                    <Button
                        intent={Intent.PRIMARY}
                        minimal
                        icon="add-column-right"
                        text="Add Column"
                        onClick={onDialogOpen}
                    />
                </Navbar.Group>
                <Navbar.Group align={Alignment.RIGHT}>
                    <Button
                        intent={Intent.DANGER}
                        minimal
                        icon="error"
                        text="Reset Table"
                        onClick={onResetData}
                    />
                </Navbar.Group>
            </Navbar>
            <div className={cn(styles.progressBarWrapper)}>
                {progressBarValue !== 0 && (
                    <ProgressBar
                        intent={progressBarValue < 1 ? Intent.PRIMARY : Intent.SUCCESS}
                        value={progressBarValue}
                    />
                )}
            </div>
            <OpviaTable />

            {isDialogOpen && (
                <AddColumnDialog
                    isOpen={isDialogOpen}
                    onClose={onDialogClose}
                    columnIndex={valuesDimensions.cols}
                    onSubmit={onSubmitColumn}
                />
            )}
        </div>
    );
};
