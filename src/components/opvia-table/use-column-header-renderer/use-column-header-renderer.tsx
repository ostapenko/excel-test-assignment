import * as React from 'react';
import { useCallback } from 'react';

import { Intent, Menu, MenuItem, Tag, Text } from '@blueprintjs/core';
import { ColumnHeaderCell2, ColumnHeaderRenderer, EditableName } from '@blueprintjs/table';
import cn from 'classnames';
import { useMap } from 'usehooks-ts';

import { IColumnType } from '../../../modules/data-storage/types';
import { ColumnHeader } from '../../../modules/table-computations-service/types';
import {
    OnColumnRename,
    OnDeleteColumn,
} from '../../../providers/table-data-provider/table-data-provider';
import styles from './styles.module.css';

export const useColumnHeaderRenderer = (
    headline: ColumnHeader[],
    onColumnRename: OnColumnRename,
    onDeleteColumn: OnDeleteColumn,
): ColumnHeaderRenderer => {
    const [intents, actions] = useMap<number, Intent>();
    const [dirtyValues, dirtyValuesActions] = useMap<number, string>();
    const onValidateName = useCallback(
        (value: string, columnIndex: number) => {
            if (value.trim().length > 2) {
                actions.remove(columnIndex);
                return true;
            } else {
                actions.set(columnIndex, Intent.DANGER);
            }
        },
        [actions, dirtyValuesActions],
    );
    const columnHeaderMenuRenderer = useCallback(
        (columnIndex: number) =>
            function RowHeaderMenu() {
                const descriptor = headline[columnIndex];
                const isLastColumn = columnIndex === headline.length - 1;
                const shouldRenderExpression = descriptor.type === IColumnType.dynamic;
                return (
                    <Menu>
                        {shouldRenderExpression && (
                            <Text
                                ellipsize
                                title={descriptor.serializedExpression}
                                className={cn(styles.expression)}
                            >
                                <code>{descriptor.serializedExpression}</code>
                            </Text>
                        )}
                        <MenuItem
                            disabled={!isLastColumn}
                            intent={Intent.DANGER}
                            icon="delete"
                            onClick={() => onDeleteColumn(columnIndex)}
                            text={`Delete column ${columnIndex + 1}`}
                        />
                    </Menu>
                );
            },
        [headline],
    );
    return useCallback(
        (columnIndex: number) => {
            const descriptor = headline[columnIndex];
            const nameRenderer = () => {
                return (
                    <div className={cn(styles.headerWrapper)}>
                        <Tag className={cn(styles.headerAlias)}>{descriptor.alias}</Tag>
                        <EditableName
                            intent={intents.get(columnIndex) || Intent.NONE}
                            className={cn(styles.headerName)}
                            index={columnIndex}
                            name={dirtyValues.get(columnIndex) ?? descriptor.name}
                            onChange={name => {
                                onValidateName(name, columnIndex);
                                dirtyValuesActions.set(columnIndex, name);
                            }}
                            onCancel={name => {
                                onValidateName(name, columnIndex);
                                dirtyValuesActions.remove(columnIndex);
                            }}
                            onConfirm={name => {
                                if (onValidateName(name, columnIndex)) {
                                    onColumnRename(name, columnIndex);
                                }

                                dirtyValuesActions.remove(columnIndex);
                                actions.remove(columnIndex);
                            }}
                        />
                    </div>
                );
            };
            return (
                <ColumnHeaderCell2
                    menuRenderer={columnHeaderMenuRenderer(columnIndex)}
                    enableColumnInteractionBar={true}
                    name={descriptor.name}
                    nameRenderer={nameRenderer}
                ></ColumnHeaderCell2>
            );
        },
        [
            headline,
            onColumnRename,
            intents,
            dirtyValues,
            dirtyValuesActions,
            actions,
            onValidateName,
        ],
    );
};
