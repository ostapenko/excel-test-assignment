import { useCallback } from 'react';

import { Cell, CellRenderer } from '@blueprintjs/table';
import cn from 'classnames';

import { ColumnAggregation, ColumnHeader } from '../../../modules/table-computations-service/types';
import styles from './styles.module.css';

export const useAggregationsTableCellRenderer = (
    headline: ColumnHeader[],
    aggregations: Array<Array<ColumnAggregation | null>>,
): CellRenderer => {
    return useCallback(
        (rowIndex: number, columnIndex: number) => {
            const maybeAggregationValue = aggregations[columnIndex][rowIndex] ?? null;

            if (!maybeAggregationValue) {
                return <Cell />;
            }

            return (
                <Cell
                    loading={maybeAggregationValue.invalidated}
                    rowIndex={rowIndex}
                    columnIndex={columnIndex}
                    className={cn(styles.cell)}
                >
                    <code>{maybeAggregationValue.name}:&nbsp;</code>
                    {String(maybeAggregationValue.value)}
                </Cell>
            );
        },
        [headline, aggregations],
    );
};
