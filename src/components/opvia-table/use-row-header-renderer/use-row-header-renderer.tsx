import * as React from 'react';
import { useCallback, useMemo } from 'react';

import { Intent, Menu, MenuItem } from '@blueprintjs/core';
import { RowHeaderCell2, RowHeaderRenderer } from '@blueprintjs/table';
import cn from 'classnames';

import { OnDeleteRow } from '../../../providers/table-data-provider/table-data-provider';
import styles from './styles.module.css';

export const useRowHeaderRenderer = (onDeleteRow: OnDeleteRow): RowHeaderRenderer => {
    const rowHeaderMenuRenderer = useCallback(
        (rowIndex: number) =>
            function RowHeaderMenu() {
                return (
                    <Menu>
                        <MenuItem
                            intent={Intent.DANGER}
                            icon="delete"
                            onClick={() => onDeleteRow(rowIndex)}
                            text={`Delete row ${rowIndex + 1}`}
                        />
                    </Menu>
                );
            },
        [onDeleteRow],
    );
    return useMemo(
        () =>
            function DefaultRowHeader(rowIndex: number) {
                return (
                    <RowHeaderCell2
                        className={cn(styles.rowHeader)}
                        index={rowIndex}
                        name={`${rowIndex + 1}`}
                        menuRenderer={rowHeaderMenuRenderer(rowIndex)}
                    />
                );
            },
        [rowHeaderMenuRenderer],
    );
};
