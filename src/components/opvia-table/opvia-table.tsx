import * as React from 'react';
import { useCallback, useState } from 'react';

import { Column, SelectionModes, Table2 } from '@blueprintjs/table';
import cn from 'classnames';

import { useTableData } from '../../providers/table-data-provider/table-data-provider';
import styles from './styles.module.css';
import { useAggregationsTableCellRenderer } from './use-aggregations-table-cell-renderer/use-aggregations-table-cell-renderer';
import { useColumnHeaderRenderer } from './use-column-header-renderer/use-column-header-renderer';
import { useMainTableCellRenderer } from './use-main-table-cell-renderer/use-main-table-cell-renderer';
import { useRowHeaderRenderer } from './use-row-header-renderer/use-row-header-renderer';

const ensureLength = <T,>(array: T[], length: number): T[] => {
    array.length = length;
    return array;
};

const TABLE_SIZES = {
    defaultRowHeight: 28,
    defaultColumnWidth: 180,
};

export const OpviaTable: React.FC = () => {
    const {
        headline,
        grid,
        aggregations,
        valuesDimensions,
        aggregationsDimensions,
        onCellValueEdit,
        onColumnRename,
        onDeleteRow,
        onDeleteColumn,
    } = useTableData();
    const mainTableCellRenderer = useMainTableCellRenderer(headline, grid, onCellValueEdit);
    const aggregationsTableCellRenderer = useAggregationsTableCellRenderer(headline, aggregations);
    const renderColumnHeader = useColumnHeaderRenderer(headline, onColumnRename, onDeleteColumn);
    const renderDefaultRowHeader = useRowHeaderRenderer(onDeleteRow);

    const [columnWidths, setColumnWidths] = useState<number[]>([]);
    const onColumnWidthChanged = useCallback(
        (index: number, size: number | undefined) => {
            setColumnWidths(widths => {
                if (typeof size === 'number') {
                    widths[index] = size;
                    return widths.slice(0);
                }
                return widths;
            });
        },
        [setColumnWidths],
    );
    return (
        <>
            <section aria-description="Table Data">
                <Table2
                    rowHeaderCellRenderer={renderDefaultRowHeader}
                    className={cn(styles.table)}
                    columnWidths={ensureLength(columnWidths, headline.length)}
                    numRows={valuesDimensions.rows}
                    cellRendererDependencies={[
                        headline,
                        grid,
                        valuesDimensions,
                        columnWidths,
                        ...grid,
                    ]}
                    enableMultipleSelection={false}
                    enableRowResizing={false}
                    enableRowReordering={false}
                    onColumnWidthChanged={onColumnWidthChanged}
                    selectionModes={SelectionModes.NONE}
                    enableGhostCells={true}
                    {...TABLE_SIZES}
                >
                    {headline.map(column => (
                        <Column
                            columnHeaderCellRenderer={renderColumnHeader}
                            id={column.alias}
                            key={column.alias}
                            cellRenderer={mainTableCellRenderer}
                            name={`${column.name} ${column.invalidated ? 'updating' : ''}`}
                        />
                    ))}
                </Table2>
            </section>
            <hr />
            <section aria-description="Aggregated Values">
                <Table2
                    rowHeaderCellRenderer={renderDefaultRowHeader}
                    className={cn(styles.table)}
                    columnWidths={ensureLength(columnWidths, headline.length)}
                    numRows={aggregationsDimensions.rows}
                    cellRendererDependencies={[
                        headline,
                        aggregations,
                        aggregationsDimensions,
                        columnWidths,
                    ]}
                    enableMultipleSelection={false}
                    enableRowResizing={false}
                    enableRowReordering={false}
                    enableColumnHeader={false}
                    onColumnWidthChanged={onColumnWidthChanged}
                    selectionModes={SelectionModes.NONE}
                    enableGhostCells={true}
                    {...TABLE_SIZES}
                >
                    {headline.map(column => (
                        <Column
                            id={column.alias}
                            key={column.alias}
                            cellRenderer={aggregationsTableCellRenderer}
                        />
                    ))}
                </Table2>
            </section>
        </>
    );
};
