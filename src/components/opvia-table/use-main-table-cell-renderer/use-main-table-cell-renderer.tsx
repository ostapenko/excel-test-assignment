import * as React from 'react';
import { useCallback } from 'react';

import { Cell, CellRenderer, EditableCell2 } from '@blueprintjs/table';
import cn from 'classnames';

import { IColumnType } from '../../../modules/data-storage/types';
import { ColumnHeader, TableValues } from '../../../modules/table-computations-service/types';
import { OnCellValueEdit } from '../../../providers/table-data-provider/table-data-provider';
import styles from './styles.module.css';

export const useMainTableCellRenderer = (
    headline: ColumnHeader[],
    grid: TableValues,
    onCellValueEdit: OnCellValueEdit,
): CellRenderer => {
    return useCallback(
        (rowIndex: number, columnIndex: number) => {
            const descriptor = headline[columnIndex];
            const cell = grid[columnIndex][rowIndex];
            switch (descriptor.type) {
                case IColumnType.dynamic: {
                    return (
                        <Cell
                            className={cn(styles.cell)}
                            loading={cell.invalidated}
                            rowIndex={rowIndex}
                            columnIndex={columnIndex}
                        >
                            {String(cell.value)}
                        </Cell>
                    );
                }
                case IColumnType.plain: {
                    return (
                        <EditableCell2
                            className={cn(styles.cell)}
                            loading={cell.invalidated}
                            rowIndex={rowIndex}
                            columnIndex={columnIndex}
                            value={String(cell.value)}
                            onConfirm={value => onCellValueEdit(value, { rowIndex, columnIndex })}
                        />
                    );
                }
                default: {
                    // eslint-disable-next-line @typescript-eslint/no-unused-vars
                    const _: never = descriptor.type;
                }
            }

            return <Cell />;
        },
        [headline, grid, onCellValueEdit],
    );
};
