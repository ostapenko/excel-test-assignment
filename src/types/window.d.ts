import DataStorageModule from '../modules/data-storage/data-storage';
import EquationsEngine from '../modules/equations-engine/equations-engine';
import TableComputationsService from '../modules/table-computations-service/table-computations-service';

declare global {
    interface Window {
        _dataStorageModule: DataStorageModule;
        _equationsEngine: EquationsEngine;
        _tableComputationsService: TableComputationsService;
    }
}
