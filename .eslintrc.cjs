module.exports = {
    extends: [
        'eslint:recommended',
        'plugin:react/recommended',
        'plugin:react/jsx-runtime',
        'plugin:@typescript-eslint/recommended',
        'plugin:@typescript-eslint/recommended-requiring-type-checking',
        'plugin:import/errors',
        'plugin:import/warnings',
        'plugin:import/typescript',
        'plugin:optimize-regex/recommended',
        'plugin:promise/recommended',
        'plugin:sonarjs/recommended',
        'prettier',
    ],
    parser: '@typescript-eslint/parser',
    parserOptions: {
        project: ['tsconfig.json', 'tsconfig.app.json'],
        ecmaVersion: 2022,
        ecmaFeatures: {
            jsx: true,
        },
        sourceType: 'module',
    },
    plugins: [
        'react',
        '@typescript-eslint',
        'import',
        'prefer-arrow',
        'simple-import-sort',
        'promise',
        'optimize-regex',
        'sonarjs',
        'prettier',
    ],
    rules: {
        '@typescript-eslint/no-unsafe-call': 'off',
        '@typescript-eslint/no-unsafe-assignment': 'off',
        '@typescript-eslint/no-unsafe-member-access': 'off',
        '@typescript-eslint/no-unsafe-argument': 'off',
        '@typescript-eslint/no-inferrable-types': 'off',
        '@typescript-eslint/no-empty-function': 'off',
        'prefer-arrow/prefer-arrow-functions': [
            'warn',
            {
                allowStandaloneDeclarations: true,
            },
        ],
        'sort-imports': 'off',
        'simple-import-sort/imports': [
            'error',
            {
                groups: [['^\\u0000'], ['^react', '^rxjs'], ['^[^.!]'], ['^\\.'], ['!']],
            },
        ],
        'simple-import-sort/exports': 'error',
        'import/first': 'error',
        'import/newline-after-import': 'error',
        'import/no-duplicates': 'error',
    },
    settings: {
        react: {
            version: '18.x',
        },
    },
    root: true,
    env: {
        browser: true,
        es6: true,
    },
};
