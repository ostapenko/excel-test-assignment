# Opvia Take-home Product Challenge

## Production deployment

Please, find the website here: https://opvia-interview.vercel.app/

## Solution details

### Useful commands

- `npm run dev` - to run the application locally
- `npm lint:all:fix` - to lint the application and fix all found errors, if possible

### Comments

The main idea that I tried to follow while completing the assignment - scalability and future proofing. Everywhere where it makes sense, I have implemented an ability to extend the functionality and/or build a foundation for future improvements and flexibility.

For example, the recalculation of columns and values is done not in one `for` cycle, but in an async pipeline of microtask, where each value is processed asynchronously, so that the interface isn't frozen during computations. Additionally, this approach allows us to move the computations to a worker or even to a serverside.

Additionally, I have implemented a storage solution for the data. Right now it stores the data in localstorage. However, because of the architecture approach I chose, it will be dead simple to adjust the code so that the data is stored on the serverside. One cool feature of my solution - the changes to the table will synchronize between the tabs ib the browser.

The list of supported functions and aggregate values is easily extensible, since all of them are moved to a config file.

#### Future improvements

Unfortunately, since I couldn't spend more time on this project, and since I was taking a bit more time on the solution than I had originally planned, I had to drop some of the features and not implement them.

- The solution doesn't support editing of the column, other than renaming, removing. Although the dialogue itself can easily be reused for editing of the column, when is called with the right props
- In a `Add new column` dialogue, the help messages are not very detailed, this could be improved
- I haven't implemented a proper error boundary
- I haven't added any unit nor e2e tests, however, I was writing the code while keeping the tests in mind. All modules use pure functions where possible. The dependencies are passed in constructors instead of importing some global values, modules
- I haven't added additional logging, performance monitoring

#### Supported features

- Add rows, remove rows via right-click on a row header
- Add columns, specify name, the list of aggregate values for the column, optionally, an expression to use to calculate the values in the column
- Rename columns by double-click on the Column header
- Delete columns by right-click on the Column header
- Change the values in the table by double-clicking on the value in the cell (works laggy due to the Blueprint internal issues)
- The changes to the table save to localstorage so that they persist even if you refresh the page
- To reset the configuration, there's a reset button


#### Task description

Congratulations on being selected for the next stage of our interview process!

We really appreciate the time you have invested in the process so far and only invited you to this next challenge because we think there’s a very good chance you’d be a great fit at Opvia. This is the penultimate step in the interview process! For context at this stage the probability of a candidate receiving an offer is (~25%).

This is our only opportunity to see what you can build so we weight it very highly. Please do show us what you can do!

## How to complete this stage of the interview process

1. Please clone this repo and use it as your starting point. This is a simple create-react-app featuring the blueprintjs table component https://blueprintjs.com/docs/#table
2. We have a prettier config to help you format your code better, but please add eslint if you wish.
3. Also feel free to restructure the code base to make it better and add missing types wherever you see fit to gain some brownie points. 🎉
4. Take your time to complete the 'Opvia product problem' below. It's up to you how you go about this!
5. Invite _jrans_ and _OliverWales_ to your own repo with your solution when you're done

## Opvia product problem

Here's the situation: scientists are using Opvia to store all their data in a standardised structure in one place. However, it's not very useful for analysis because scientists can't yet apply formulas to the data. You've told a customer that you're building this feature and you're going to demo it to them...

Make it possible for users to add `calculation columns`:

- The user should be able to add new derived properties of a record by writing a formula that can specify other columns to calculate with.
- You've identified with the user that these formulas require basic math operations (addition, multiplication etc.) (e.g. multiplying cell density column and volume column -> cell count)

Make it possible for users to add `table aggregations`:

- Provide a way for a user to calculate aggregate values, for instance the mean volume across all records, or the sum of all the cell counts.

### What we're looking for

Something that meets the above minimal requirements and can act as a starting point to get more feedback on.

We are not looking for an Excel copy. These are a table of records not a spreadsheet.

Unsure whether to submit? Would you happily sit down with a scientist and show them what you've built? Would what you've showed them make them more excited about using Opvia?

Ran out of time? Document any features that you'd like to have built.

#### FAQS

- Can I change the structure/content of the raw data? - yes feel free to, but don't feel obligated to (this is a product not and engineering challenge)

#### Any questions

If you have any questions, or anything is unclear please email Jack at `jack.rans@opvia.bio`
